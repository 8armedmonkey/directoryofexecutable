/**
 * Test 1: gradlew clean run
 * Test 2: gradlew clean fatJar && java -jar <fat-jar-path>
 */
fun main() {
    val cls = object : Any() {}.javaClass

    println("Executable path 1: ${Environment.getExecutableDirectory(cls).absolutePath}")
    println("Executable path 2: ${Environment.getExecutableDirectory().absolutePath}")
}