import java.io.File

object Environment {

    fun getExecutableDirectory(cls: Class<*> = javaClass): File =
            File(cls.protectionDomain.codeSource.location.toURI())

}